
const axios = require('axios');
var dateFormat = require('dateformat');



const conexion = () => {
    const { Pool } = require("pg");
    var pool = new Pool({
        user: "postgres",
        password: "rpalatam20",
        database: "AWS_ElComercio",
        port: 5432,
        host: "database.cpehi2ylfzlh.us-east-1.rds.amazonaws.com",
        ssl: { rejectUnauthorized: false },
    });
    return pool;
};

function responseMessage(results) {
    let statusCode = 200;
    let data = results;
    let success = true;

    return {
        statusCode,
        headers: {
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Credentials": true,
            Accept: "*/*",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ success, data }),
    };
}

module.exports.handler = async (event, context) => {
    var requestBody = JSON.parse(event.body);

    var id = requestBody.idsuscripcion;
    var id_subcategoria_chatbot = requestBody.subcategoria;


    var message = "";
    var code = "";




    const response = await new Promise((rsv, rjt) => {

        try {
            if (id_subcategoria_chatbot == "1") {
                let hdrs = {};
                hdrs.Authorization =
                    "Bearer 109998623fcffa2086f406f8880b24942532d80fc5da0420047ab1ee22f6bdfa";

                axios
                    .get(
                        'https://jjnkyl1a6f.execute-api.us-west-2.amazonaws.com/test/api/v1/subscription-print/' + id + '/details',
                        {
                            headers: hdrs,
                        }
                    )
                    .then(function (response) {

                        var data = response['data'];


                        if (data) {
                            message = "success";
                            var productos = response['data']['name'];
                            var periodo = response['data']['periodicity'];
                            var pagomensual = response['data']['price'];
                            var diasentregadiario = response['data']['deliveryDays'];
                            var importepagopendiente = response['data']['lastPaymentAmount'];
                            var fechaultimopagocancelado = response['data']['lastPaymentDate'];
                            var fechasiguienterenovacion = response['data']['nextPaymentDate'];
                            var importeultimopagocancelado = response['data']['nextPaymentAmount'];
                            //var nextPaymentAmount = response['data']['nextPaymentAmount'];
                            code = "1";
                            console.log(code);
                            console.log(message);

                        } else {

                            message = "No se encontro ningun informacion de paquete";
                            code = "0";
                            console.log(message);
                            console.log(code);
                        }


                        rsv({
                            message,
                            productos,
                            periodo,
                            pagomensual,
                            diasentregadiario,
                            importepagopendiente,
                            fechaultimopagocancelado,
                            fechasiguienterenovacion,
                            importeultimopagocancelado,
                            code,
                        });



                    })
                    .catch(function (error) {
                        rjt({
                            message: error.message,
                            code: '0'
                        });
                    })

            } else if (id_subcategoria_chatbot == "2") {
                let hdrs = {};
                hdrs.Authorization =
                    "Bearer 109998623fcffa2086f406f8880b24942532d80fc5da0420047ab1ee22f6bdfa";

                axios
                    .get(
                        'https://jjnkyl1a6f.execute-api.us-west-2.amazonaws.com/test/api/v1/subscription-bundle/' + id + '/details',
                        {
                            headers: hdrs,
                        }
                    )
                    .then(function (response) {

                        var data = response['data'];


                        if (data) {
                            message = "success";
                            var productos = response['data']['name'];
                            var periodo = response['data']['periodicity'];
                            var pagomensual = response['data']['price'];
                            var diasentregadiario = response['data']['deliveryDays'];
                            var importepagopendiente = response['data']['lastPaymentAmount'];
                            var fechaultimopagocancelado = response['data']['lastPaymentDate'];
                            var fechasiguienterenovacion = response['data']['nextPaymentDate'];
                            var importeultimopagocancelado = response['data']['nextPaymentAmount'];
                            code = "1";
                            console.log(code);
                            console.log(message);

                        } else {

                            message = "No se encontro ningun informacion de paquete";
                            code = "0";
                            console.log(message);
                            console.log(code);
                        }


                        rsv({
                            message,
                            productos,
                            periodo,
                            pagomensual,
                            diasentregadiario,
                            importepagopendiente,
                            fechaultimopagocancelado,
                            fechasiguienterenovacion,
                            importeultimopagocancelado,
                            code,
                        });

                    })
                    .catch(function (error) {
                        rjt({
                            message: error.message,
                            code: '0'
                        });
                    })
            } else if (id_subcategoria_chatbot == "3") {

                let hdrs = {};
                hdrs.Authorization =
                    "Bearer 109998623fcffa2086f406f8880b24942532d80fc5da0420047ab1ee22f6bdfa";

                axios
                    .get(
                        'https://jjnkyl1a6f.execute-api.us-west-2.amazonaws.com/test/api/v1/subscription-digital/' + id + '/details',
                        {
                            headers: hdrs,
                        }
                    )
                    .then(function (response) {



                        var data = response['data'];


                        if (data) {
                            message = "success";
                            var productos = response['data']['name'];
                            var periodo = response['data']['periodicity'];
                            var pagomensual = response['data']['price'];
                            var diasentregadiario = response['data']['deliveryDays'];
                            var importepagopendiente = response['data']['lastPaymentAmount'];
                            var fechaultimopagocancelado = response['data']['lastPaymentDate'];
                            var fechasiguienterenovacion = response['data']['nextPaymentDate'];
                            var importeultimopagocancelado = response['data']['nextPaymentAmount'];
                            code = "1";
                            console.log(code);
                            console.log(message);

                        } else {

                            message = "No se encontro ningun informacion de paquete";
                            code = "0";
                            console.log(message);
                            console.log(code);
                        }


                        rsv({
                            message,
                            productos,
                            periodo,
                            pagomensual,
                            diasentregadiario,
                            importepagopendiente,
                            fechaultimopagocancelado,
                            fechasiguienterenovacion,
                            importeultimopagocancelado,
                            code,
                        });

                    })
                    .catch(function (error) {
                        rjt({
                            message: error.message,
                            code: '0'
                        });
                    })
            } else {
                rjt({
                    message: 'No existe la category',
                    code: '0'
                });
            }
        } catch (error) {
            rjt({
                message: error.message,
                code: '0'
            });
        }
    });

    return responseMessage(response);
};


/*
function saveConsulta(nrodocumento, nrodelivery) {
    const con = conexion();
    var sql = "INSERT INTO tipificacion_bot (dni,observacion,tipo,estado,nro_delivery,motivo,submotivo) VALUES ('" + nrodocumento + "','Consulta periodo, dias de reparto, direccion','LLAMADAS- 25','0', '" + nrodelivery + "', 'CONSULTAS','CONSULTA DE FACTURACION')";
    con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("se inserto en la tabla tipificacion_bot ");
    });

    freshdesk.createTicket({
        name: nombre,
        email: correo,
        subject: 'CONSULTA INFORMATIVA GENERAL DE PAQUETE',
        description: 'CONSULTA INFORMATIVA GENERAL DE PAQUETE',
        status: 2,
        priority: 1
    }, function (err, data) {
        //console.log(err || data)
    })

}
*/